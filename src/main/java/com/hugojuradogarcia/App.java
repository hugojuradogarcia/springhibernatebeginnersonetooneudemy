package com.hugojuradogarcia;

import com.hugojuradogarcia.controllers.CourseController;
import com.hugojuradogarcia.controllers.InstructorController;
import com.hugojuradogarcia.entities.Course;
import com.hugojuradogarcia.entities.Instructor;
import com.hugojuradogarcia.entities.InstructorDetail;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        InstructorController instructorController =  new InstructorController();
        CourseController courseController =  new CourseController();

        Instructor instructor = new Instructor("Mila", "Aretia", "mila@gmail.com");
        InstructorDetail instructorDetail =  new InstructorDetail("http://youtube", "Udemy mila");

        Course course =  new Course("C#");
        Course course1 =  new Course("JS");

        // Get instructor
        Instructor instructor1 = instructorController.getInstructor(1);
        Instructor instructor2 = instructorController.getInstructorQuery(1);
        instructor1.add(course);
        instructor1.add(course1);

        // Get Instructor Course
        courseController.getInstructorCourse(1);

        // Delete course
        //courseController.deleteCourse(10);

        // Save Course
        // courseController.saveCourse(course);
        // courseController.saveCourse(course1);

        instructor.setInstructorDetail(instructorDetail);

        // Save
        //instructorController.saveInstructor(instructor);

        // Delete
        //instructorController.deleteInstructor(1);

        // Find By Id bi
        //instructorController.getInstructorDetail(2);

        // Delete InstructorDetail
        //instructorController.deleteInstructorDetail(4);
    }
}
