package com.hugojuradogarcia.controllers;

import com.hugojuradogarcia.entities.Course;
import com.hugojuradogarcia.entities.Instructor;
import com.hugojuradogarcia.entities.InstructorDetail;
import com.hugojuradogarcia.utils.HibernateUtils;
import org.hibernate.Session;

public class CourseController {
    public void saveCourse(Course course){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Creating new instructor & instructorDetail object");

            session.beginTransaction();
            session.save(course);
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void getInstructorCourse(int idInstructor){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Get Instructor Course");

            session.beginTransaction();
            Instructor instructor = session.get(Instructor.class, idInstructor);
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void deleteCourse(int idCourse){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Delete course detail with id: " + idCourse);

            session.beginTransaction();
            Course course = session.get(Course.class, idCourse);
            if (course != null) {
                session.delete(course);
            }

            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
