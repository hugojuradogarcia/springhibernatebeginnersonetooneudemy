package com.hugojuradogarcia.controllers;

import com.hugojuradogarcia.entities.Instructor;
import com.hugojuradogarcia.entities.InstructorDetail;
import com.hugojuradogarcia.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class InstructorController {

    public void saveInstructor(Instructor instructor){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Creating new instructor & instructorDetail object");

            session.beginTransaction();
            session.save(instructor);
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }



    public void deleteInstructor(int idInstructor){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Delete instructor with id: " + idInstructor);

            session.beginTransaction();
            Instructor instructor = session.get(Instructor.class, idInstructor);
            if (instructor != null) {
                session.delete(instructor);
            }

            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void getInstructorDetail(int idInstructorDetails){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Get instructor Details with id: " + idInstructorDetails);

            session.beginTransaction();
            InstructorDetail instructorDetail = session.get(InstructorDetail.class, idInstructorDetails);
            System.out.println(instructorDetail.getInstructor().getFirstName());
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void deleteInstructorDetail(int idInstructorDetail){

        Session session = HibernateUtils.getSessionFactory().openSession();

        try {
            System.out.println("Delete instructor detail with id: " + idInstructorDetail);

            session.beginTransaction();
            InstructorDetail instructorDetail = session.get(InstructorDetail.class, idInstructorDetail);
            instructorDetail.getInstructor().setInstructorDetail(null);
            if (instructorDetail != null) {
                session.delete(instructorDetail);
            }

            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public Instructor getInstructor(int idInstructor){

        Session session = HibernateUtils.getSessionFactory().openSession();
        Instructor instructor = new Instructor();

        try {
            System.out.println("Vet instructor detail with id: " + idInstructor);
            session.beginTransaction();
            instructor = session.get(Instructor.class, idInstructor);
            System.out.println("Instructor: " + instructor.toString());
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }

        return instructor;
    }

    public Instructor getInstructorQuery(int idInstructor){

        Session session = HibernateUtils.getSessionFactory().openSession();
        Instructor instructor = new Instructor();

        try {
            System.out.println("Get instructor query with id: " + idInstructor);
            session.beginTransaction();

            Query<Instructor> query = session.createQuery("select i from Instructor i "
                    + "JOIN FETCH i.courses "
                    + "where i.id =: theInstructorId", Instructor.class);
            query.setParameter("theInstructorId", idInstructor);

            instructor =  query.getSingleResult();

            System.out.println("Instructor: " + instructor.toString());
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }

        return instructor;
    }
}
